# fastcgi

## Official documentation
* fastcgi.com offline

## Unofficial documentation
* [*FastCGI Specification*](http://www.mit.edu/~yandros/doc/specs/fcgi-spec.html)
  1996 Mark R. Brown

# Websocket issue
* Some people report that Fastcgi may not support Websocket.
* [*node-fastcgi*
  ](https://www.npmjs.com/package/node-fastcgi)
  * Typically upgrade/websocket requests won't work with FastCGI applications because of input/output buffering.

## Replacements
* [NGINX Unit](https://unit.nginx.org)
  * Go: by overloading the http module
  * JavaScript (Node.js): by overloading the http and websocket modules
  * Java: via the Servlet Specification 3.1 and WebSocket APIs
  * Perl: via PSGI
  * PHP: via the embed SAPI
  * Python: via WSGI and ASGI with WebSocket support
  * Ruby: via the Rack API
* uWsgi
  * WSGI, PSGI, Rack, Lua WSAPI, CGI, PHP, Go …
  * [Supported languages and platforms](https://uwsgi-docs.readthedocs.io/en/latest/LanguagesAndPlatforms.html)
  * Currently, you can write plugins in C, C++ and Objective-C. (readthedocs, 2020-12)
2020-12

# Libraries for web-sites back-ends
## Perl
...

## PHP (client and server libraries)
* https://phppackages.org/s/fastcgi

## FastCGI Process Managers
* fastcgi manager site:debian.org
* PHP...

## ASP.NET
* https://packages.debian.org/sid/mono-fpm-server
